package jp.alhinc.matsunaga_nana.calculate_sales;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args){

		try{
			//マップの宣言
			Map<String,String>branchesMap = new HashMap<>();    //支店定義ファイルを保持するマップ
			Map<String,Long>salesMap = new HashMap<>();    //売上ファイルを保持するマップ


			BufferedReader branchesBr = null;

			try{
				branchesBr = new BufferedReader(new FileReader(new File(args[0],"branch.lst")));     //支店ファイル読み込み

				String line;
				while((line = branchesBr.readLine()) != null){    //1行ずつ読み込み
					String[] branchesData = line.split(",");    //行をカンマ区切りで配列に変換

					int size = branchesData.length;
					if(! branchesData[0].matches("[0-9]{3}") || size != 2 || branchesData[1].equals(" ")){
						//支店コードが数字3桁以外、または支店名にカンマや改行を含む場合
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return;
					}

					branchesMap.put(branchesData[0],branchesData[1]);    //読み込んだデータを支店ファイルマップに保持
					salesMap.put(branchesData[0],(long)0);    //読み込んだデータを売上ファイルマップに保持
				}
			}catch(FileNotFoundException e){
				System.out.println("支店定義ファイルが存在しません");
				return;
			}finally{
				if(branchesBr != null){
					branchesBr.close();
				}
			}


			BufferedReader salesBr = null;

			try{
				File directory = new File(args[0]);
				ArrayList<Integer>numbers = new ArrayList<Integer>();

				for(String fileName : directory.list()){
					if(fileName.matches("^\\d{8}.rcd$")){     //ファイル名が8桁、拡張子.rcd

						String[] numberList = fileName.split(".rcd");    //行を".rcd"区切りで配列に変換
						Integer number = Integer.parseInt(numberList[0]);    //parseIntで数値に変換
						numbers.add(number);
					}
				}

				Collections.sort(numbers);    //ファイル名を昇順に並び替え
				Integer min = numbers.get(0);
				Integer index = numbers.size() - 1;
				Integer max = numbers.get(index);

				if(min + index != max){
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}

				for(String fileName : directory.list()){
					if(fileName.matches("^\\d{8}.rcd$")){     //ファイル名が8桁、拡張子.rcd

						salesBr = new BufferedReader(new FileReader(new File(args[0],fileName)));    //売上ファイルの読み込み
						String code = salesBr.readLine();    //1行目を読み込み
						Long salesdata = Long.parseLong(salesBr.readLine());    //2行目を読み込み、売上データをInt型に変換
						String third = salesBr.readLine();

						if(third != null){    //売上ファイルの中身が3行以上ある場合
							System.out.println(fileName + "のフォーマットが不正です");
							return;
						}
						if(! salesMap.containsKey(code)){    //支店に該当がなかった場合
							System.out.println(fileName + "の支店コードが不正です");
							return;
						}

						Long sale = salesMap.get(code);
						Long sum = sale + salesdata;    //売り上げファイルから抽出したデータを合算

						salesMap.put(code,sum);    //データを売上ファイルマップに保持

						for(Long value : salesMap.values()){
							String letter = Long.toString(value);    //toStringで文字列へ変換
							if(letter.matches("[0-9]{10,}")){
								System.out.println("合計金額が10桁を超えました");
								return;
							}
						}
					}
				}
			}finally{
				if(salesBr != null){
					salesBr.close();
				}
			}

			PrintWriter pw = null;


			pw = new PrintWriter(new FileWriter(new File(args[0],"branch.out")));

			for(Map.Entry<String,String>entry : branchesMap.entrySet()){
				String code = entry.getKey();
				Long total = salesMap.get(code);
				String collections = code + "," + entry.getValue() + "," + total;

				pw.println(collections);     //ファイルに書き込む
				pw.close();    //ファイルを閉じる
			}
		}catch(Exception e){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
	}
}
